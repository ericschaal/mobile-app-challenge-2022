# MY01 Mobile App Challenge

Welcome to the Mobile Application technical challenge!

This repository contains a single README file.
Start by forking this repo to a **private** personal repository. You can use Gitlab, Github, or any other collaborative version control tool.

## The challenge!

Build a cross-platform Bluetooth scanner using React Native.

Software requirements are as follows:
- Users shall be able to scan for nearby, advertising, Bluetooth devices.
- Users shall be able to refresh the scan result.
- The application shall display relevant information for each scanned peripheral.
- User interface shall be intuitive.

### Example
![Example](https://cdn.dribbble.com/users/153967/screenshots/2966913/media/1164945bce832ce0568354e11ad3125b.gif)

### Bonus 1
Develop unit, component, and integration tests.

### Bonus 2
Configure a CI pipeline that builds your app and runs associated tests.

## Developing a Cross-Platform React Native Application
**This challenge does not require an Apple computer or a physical phone.**
- 🍎 If you own an Apple computer you have the choice to compile and test your application on an iPhone or an Android device (physical or simulator).
- 🤖 If you don't own an Apple computer, you should develop and test your application on an Android device (physical or emulator).
- If you don't own a physical iOS/Android device, you should use a simulator/emulator to test and develop your app.

If you are not able to test/run your application on both platforms, let us know, you won't be penalized.

## General Guidelines

- **Only submit your own work.** 
- You are allowed to use any framework/library. But keep in mind that we might ask you to justify their use during the follow-up interview.
- You are allowed to use Expo CLI but you might need to "eject" the app to integrate a native Bluetooth library.
- Bonuses are optional.
- Feel free to ask questions, email us or contact us on LinkedIn.

## Submission
Once satisfied with your work, invite both of us
- Eric Schaal: [Github](https://github.com/ericschaal) or [Gitlab](https://gitlab.com/ericschaal)
- Alexandre Cassange [Github](https://github.com/AlexandreCassagne) or [Gitlab](https://gitlab.com/AlexandreCassagne)

as collaborators (read-only is good enough) to your repository. Then email us or contact us on LinkedIn.

## Useful Websites (Maybe)
 - [React Native](https://reactnative.dev): Create native apps for Android and iOS using React
 - [Expo](https://expo.dev): Expo is a framework and a platform for universal React applications.
 - [Redux](https://redux.js.org): A Predictable State Container for JS Apps
 - [Redux Toolkit](https://redux-toolkit.js.org): The official, opinionated, batteries-included toolset for efficient Redux development
 - [MobX](https://mobx.js.org/README.html): Simple, scalable state management. 
 - [Jest](https://jestjs.io): Jest is a delightful JavaScript Testing Framework with a focus on simplicity.
 - [Dribbble](https://dribbble.com): Dribbble is a great destination to find creative app design ideas. 

 # FEEL FREE TO EDIT THIS FILE

 ## How to run your App?
 ## Screenshots
 ### Etc..
